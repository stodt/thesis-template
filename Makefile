thesis.pdf: thesis.tex
	latexmk -shell-escape -lualatex thesis.tex

clean:
	@echo "Cleaning temp files"
	@rm thesis.log thesis.aux thesis.toc thesis.fls thesis.fdb_latexmk thesis.blg thesis.bbl 2> /dev/null || true
